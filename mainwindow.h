#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QMessageBox>
#include "gamegraphicsscene.h"
#include "newgamedialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Ui::MainWindow *getUi();
    
private slots:
    void on_newAction_triggered();
    void on_aboutAction_triggered();

private:
    Ui::MainWindow *ui;
    GameGraphicsScene *scene;
};

#endif // MAINWINDOW_H
