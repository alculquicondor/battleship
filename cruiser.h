#ifndef CRuISER_H
#define CRUISER_H

#include "ship.h"

class Cruiser: public Ship
{
public:
    static const int LENGTH = 3;
    Cruiser(int row,int column,bool direction);
private:
    void f() {}
};

#endif // CRUSIER_H
