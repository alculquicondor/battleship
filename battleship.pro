#-------------------------------------------------
#
# Project created by QtCreator 2013-05-29T11:03:20
#
#-------------------------------------------------

QT       += core gui phonon

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = battleship
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tablepainterpath.cpp \
    gamegraphicsscene.cpp \
    animatedpictureitem.cpp \
    ship.cpp \
    attackresult.cpp \
    newgamedialog.cpp \
    battleshipengine.cpp \
    battleship.cpp \
    submarine.cpp \
    destroyer.cpp \
    cruiser.cpp

HEADERS  += mainwindow.h \
    tablepainterpath.h \
    gamegraphicsscene.h \
    animatedpictureitem.h \
    ship.h \
    attackresult.h \
    newgamedialog.h \
    battleshipengine.h \
    battleship.h \
    submarine.h \
    destroyer.h \
    cruiser.h

FORMS    += mainwindow.ui \
    newgamedialog.ui

RESOURCES += \
    resources.qrc
