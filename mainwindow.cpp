#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->gameGraphicsView->setRenderHint(QPainter::Antialiasing);
    scene = new GameGraphicsScene(this);
    ui->gameGraphicsView->setScene(scene);
    connect(scene, SIGNAL(statusMessage(QString)), ui->statusBar, SLOT(showMessage(QString)));
    connect(scene, SIGNAL(statusMessage(QString, int)), ui->statusBar, SLOT(showMessage(QString, int)));
}

MainWindow::~MainWindow()
{
    delete scene;
    delete ui;
}

Ui::MainWindow *MainWindow::getUi()
{
    return ui;
}

void MainWindow::on_newAction_triggered()
{
   NewGameDialog *newGameDialog = new NewGameDialog(this);
   newGameDialog->setModal(true);
   if (newGameDialog->exec() == QDialog::Accepted) {
       int rows = newGameDialog->getRows(), columns = newGameDialog->getColumns();
       scene->setRowsColumns(rows, columns);
   }
}

void MainWindow::on_aboutAction_triggered()
{
    QMessageBox::about(this, "Battleship",
                       "Creado por:\nAldo Culquicondor\nYanlui Alarcon");
}
