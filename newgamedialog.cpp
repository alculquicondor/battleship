#include "newgamedialog.h"
#include "ui_newgamedialog.h"

NewGameDialog::NewGameDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewGameDialog)
{
    ui->setupUi(this);
}

NewGameDialog::~NewGameDialog()
{
    delete ui;
}

int NewGameDialog::getRows()
{
    return ui->rowsLineEdit->text().toInt();
}

int NewGameDialog::getColumns()
{
    return ui->columnsLineEdit->text().toInt();
}
