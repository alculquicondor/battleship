#ifndef ATTACKRESULT_H
#define ATTACKRESULT_H

#include "ship.h"

class AttackResult
{
public:

    enum result {
        INVALID,
        VALID,
        HIT,
        WIN,
    };

    explicit AttackResult(result state = INVALID, Ship *destroyedShip = 0);
    result state;
    Ship *destroyedShip;

    
};

#endif // ATTACKRESULT_H
