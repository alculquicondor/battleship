#ifndef ANIMATEDPICTUREITEM_H
#define ANIMATEDPICTUREITEM_H

#include <QGraphicsPixmapItem>
#include <QPropertyAnimation>

class AnimatedPictureItem : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF pos READ pos WRITE setPos)
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity)
public:
    enum
    {
        AnimatedPictureItemType = UserType + 1
    };
    AnimatedPictureItem(QSizeF size, const QPixmap& pixmap, QObject* parent = 0);
    ~AnimatedPictureItem();

    void animatePosition(const QPointF& start, const QPointF& end);
    void animateOpacity(const qreal& start, const qreal& end, int mtime = 1000);

    QRectF boundingRect() const;

    int type() const
    {
        return AnimatedPictureItemType;
    }

signals:
    void animationFinishedSignal();

public slots:
    void animationFinished();

private:
    QSizeF m_size;

};

#endif // ANIMATEDPICTUREITEM_H
