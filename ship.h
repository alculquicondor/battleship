#ifndef SHIP_H
#define SHIP_H

#include <QString>
#include <QPoint>
#include <QPixmap>

class Ship
{
public:

    explicit Ship();
    explicit Ship(QString name, int length, int row, int column, bool direction);
    QString name;
    virtual ~Ship();
    QString getName();
    int getLength();
    bool getDirection();
    QPoint getPosition();
    bool attacked();
    QPixmap *getIcon();

protected:
    virtual void f() = 0;

private:

    int length;
    int row;
    int column;
    int cellsLeft;
    bool direction;
    QPixmap *icon;
};

#endif // SHIP_H
