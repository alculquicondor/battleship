#include "ship.h"
#include "battleship.h"

Ship::Ship()
{
}

Ship::Ship(QString name, int length, int row, int column, bool direction) :
        name(name),length(length),row(row),column(column),direction(direction)
{
    icon = new QPixmap(":/resources/img/" + name + ".png");
    cellsLeft = length;
}

Ship::~Ship()
{
    delete icon;
}

QString Ship::getName()
{
    return name;
}

bool Ship::attacked()
{
   cellsLeft--;
   return cellsLeft == 0;
}

QPixmap *Ship::getIcon()
{
    return icon;
}

bool Ship::getDirection()
{
    return direction;
}

QPoint Ship::getPosition()
{
    return QPoint(row,column);
}

int Ship::getLength()
{
    return length;
}

