#ifndef TABLEPAINTERPATH_H
#define TABLEPAINTERPATH_H

#include <QPainterPath>
#include <QFont>
#include <algorithm>

class TablePainterPath : public QPainterPath
{
public:
    explicit TablePainterPath(int rows, int columns);

private:
    static QString numberToAlphabet(int number);
    static const int cellSize = 32;
};

#endif // TABLEPAINTERPATH_H
