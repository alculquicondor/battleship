#ifndef SUBMARINE_H
#define SUBMARINE_H

#include "ship.h"

class Submarine : public Ship
{
public:
    static const int LENGTH = 4;
    Submarine(int row,int column,bool direction);
private:
    void f() {}
};

#endif // SUBMARINE_H
