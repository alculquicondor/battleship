#include "tablepainterpath.h"

TablePainterPath::TablePainterPath(int rows, int columns)
{
    addRect(cellSize, cellSize, cellSize * columns, cellSize * rows);
    for (int i = 1; i < columns; i++) {
        addText(cellSize * i + 10, 20, QFont(), numberToAlphabet(i-1));
        moveTo(cellSize * (i+1), cellSize);
        lineTo(cellSize * (i+1), cellSize * (rows+1));
    }
    addText(cellSize * columns + 10, 20, QFont(), numberToAlphabet(columns-1));
    for (int i = 1; i < rows; i++) {
        addText(4, cellSize * i + 20, QFont(), QString::number(i));
        moveTo(cellSize, cellSize * (i+1));
        lineTo(cellSize * (columns+1), cellSize * (i+1));
    }
    addText(4, cellSize * rows + 20, QFont(), QString::number(rows));
}

QString TablePainterPath::numberToAlphabet(int number)
{
    QString result;
    while (number >= 0) {
        result.push_back('A'+(number%26));
        number = number / 26 - 1;
    }
    std::reverse(result.begin(), result.end());
    return result;
}
