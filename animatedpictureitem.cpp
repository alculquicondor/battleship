#include "animatedpictureitem.h"

AnimatedPictureItem::AnimatedPictureItem(QSizeF size, const QPixmap& pixmap, QObject* parent) :
    QObject(parent), QGraphicsPixmapItem(pixmap)
{
    m_size = size;
    setCacheMode(DeviceCoordinateCache);
}

AnimatedPictureItem::~AnimatedPictureItem()
{
}

QRectF AnimatedPictureItem::boundingRect() const
{
    return QRectF(0, 0, m_size.width(), m_size.height());
}

void AnimatedPictureItem::animatePosition(const QPointF& start, const QPointF& end)
{
    QPropertyAnimation* anim = new QPropertyAnimation(this, "pos");

    anim->setDuration(800);
    anim->setStartValue(start);
    anim->setEndValue(end);
    anim->setEasingCurve(QEasingCurve::InBack);

    QObject::connect(anim, SIGNAL(finished()), this, SLOT(animationFinished()));

    anim->start(QAbstractAnimation::DeleteWhenStopped);
}

void AnimatedPictureItem::animateOpacity(const qreal &start, const qreal &end, int mtime)
{
    QPropertyAnimation *anim = new QPropertyAnimation(this, "opacity");
    anim->setDuration(mtime);
    anim->setStartValue(start);
    anim->setEndValue(end);
    anim->setEasingCurve(QEasingCurve::InElastic);

    QObject::connect(anim, SIGNAL(finished()), this, SLOT(animationFinished()));

    anim->start(QAbstractAnimation::DeleteWhenStopped);
}

void AnimatedPictureItem::animationFinished()
{
    emit animationFinishedSignal();
}
