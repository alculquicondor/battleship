#ifndef GAMEGRAPHICSSCENE_H
#define GAMEGRAPHICSSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <phonon/MediaSource>
#include <phonon/MediaObject>
#include "tablepainterpath.h"
#include "animatedpictureitem.h"
#include "battleshipengine.h"
#include "attackresult.h"
#include "ship.h"

class GameGraphicsScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit GameGraphicsScene(QObject *parent = 0, int rows = 15, int columns = 15);
    ~GameGraphicsScene();
    void setRowsColumns(int rows, int columns);

signals:
    void statusMessage(QString);
    void statusMessage(QString, int);

public slots:
    void soundFinishedPlaying();
    void soundAboutToFinish();
    void showExplosion();
    void showSplash();

private:
    static const int cellSize = 32;
    int rows, columns;
    QPen *tablePen;
    QPixmap *explosionIcon, *missileIcon, *splashIcon, *winIcon;
    BattleShipEngine *engine;
    Phonon::MediaSource *releaseSound, *explodeSound, *splashSound;
    bool finished;

    void mousePressEvent(QGraphicsSceneMouseEvent *);
    void play(int row, int column);
    void replaceNAnimate(AnimatedPictureItem *missile, QPixmap *icon);
};

#endif // GAMEGRAPHICSSCENE_H
