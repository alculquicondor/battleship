#ifndef DESTROYER_H
#define DESTROYER_H

#include "ship.h"

class Destroyer : public Ship
{
public:
    static const int LENGTH = 3;
    Destroyer(int row,int column,bool direction);
private:
    void f() {}
};

#endif // DESTROYER_H
