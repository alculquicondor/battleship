#include "battleshipengine.h"
#include "battleship.h"

BattleShipEngine::BattleShipEngine(int rows, int columns) :
    rows(rows), columns(columns)
{
    qsrand(QTime::currentTime().msec());
    attacksMatrix  = new bool*[rows];
    for(int i = 0 ; i < rows ; i++)
    {
        attacksMatrix[i] = new bool[columns];
        for(int j = 0 ; j < columns ; j++){
            attacksMatrix[i][j] = false;
        }
    }

    shipsMatrix = new int*[rows];
    for(int i = 0 ; i < rows ; i++){
        shipsMatrix[i] = new int[columns];
        for(int j = 0 ; j < columns ; j++){
            shipsMatrix[i][j] = -1;
        }
    }
    shot = 0;
}

void BattleShipEngine::putShips(int amount)
{
    int lengths[] = {BattleShip::LENGTH, Cruiser::LENGTH,
                     Destroyer::LENGTH, Submarine::LENGTH};
    ships.reserve(amount);
    int put = 0;
    for (int i = 0; i < amount; i++) {
        bool valid = false;
        for (int j = 0; not valid and j < 50; j++) {
            int row = qrand() % rows, column = qrand() % columns;
            bool direction = qrand() % 2;
            valid = verifySpace(row, column, direction, lengths[i%4]);
            ship = new constructores[i%4](rows, column, direction);

            if (valid) {
                Ship *ship;
                switch (i % 4) {
                case 0:
                    ship = new BattleShip(row, column, direction);
                    break;
                case 1:
                    ship = new Cruiser(row, column, direction);
                    break;
                case 2:
                    ship = new Destroyer(row, column, direction);
                    break;
                case 3:
                    ship = new Submarine(row, column, direction);
                }
                fillWithShip(ship, put);
                ships.push_back(ship);
                put ++;
            }
        }
    }
    shipsLeft = put;
}


AttackResult BattleShipEngine::attack(int row, int column)
{
    if (attacksMatrix[row][column]){
        return AttackResult();
    }
    shot++;
    attacksMatrix[row][column] = true;
    if (shipsMatrix[row][column] < 0){
        return AttackResult(AttackResult::VALID);
    }
    Ship *ship = ships[shipsMatrix[row][column]];
    AttackResult result(AttackResult::HIT);
    if(ship->attacked()){
        result.destroyedShip = ship;
        shipsLeft--;
        if(shipsLeft == 0)
            result.state = AttackResult::WIN;
    }
    return result;
}

bool BattleShipEngine::verifySpace(int row, int column, bool direction, int length)
{
    if (direction) {
        if (row + length > rows)
            return false;
        for (int r = row; r < row + length; r++)
            if (shipsMatrix[r][column] >= 0)
                return false;
    } else {
        if (column + length > columns)
            return false;
        for (int c = column; c < column + length; c++)
            if (shipsMatrix[row][c] >= 0)
                return false;
    }
    return true;
}

void BattleShipEngine::fillWithShip(Ship *ship, int id)
{
    bool direction = ship->getDirection();
    QPoint position = ship->getPosition();
    int length = ship->getLength();
    if (direction) {
        for (int r = position.x(); r < position.x() + length; r++)
            shipsMatrix[r][position.y()] = id;
    } else {
        for (int c = position.y(); c < position.y() + length; c++)
            shipsMatrix[position.x()][c] = id;
    }
}


BattleShipEngine::~BattleShipEngine()
{
    for(int i  = 0 ; i < rows ; i++){
        delete []attacksMatrix[i];
    }
    delete []attacksMatrix;

    for(int i  = 0 ; i < rows ; i++){
        delete []shipsMatrix[i];
    }
    delete []shipsMatrix;

    for(int i = 0 ; i < ships.size() ; i++){
        delete ships[i];
    }
}


int BattleShipEngine::getShipsLeft()
{
    return shipsLeft;
}

