#ifndef BATTLESHIPENGINE_H
#define BATTLESHIPENGINE_H

#include <QVector>
#include <QTime>
#include <QtGlobal>
#include "attackresult.h"
#include "ship.h"
#include "battleship.h"
#include "cruiser.h"
#include "submarine.h"
#include "destroyer.h"

class BattleShipEngine
{
public:

    explicit BattleShipEngine(int rows, int columns);
    void putShips(int amount = 4);
    int getShips();
    int shot;
    int getShipsLeft();
    ~BattleShipEngine();
    AttackResult attack(int row, int column);

private:
    int rows;
    int columns;
    bool** attacksMatrix;
    int** shipsMatrix;
    int shipsLeft;
    QVector<Ship*> ships;
    bool verifySpace(int row, int column, bool direction, int length);
    void fillWithShip(Ship *ship, int id);
};

#endif // BATTLESHIPENGINE_H
