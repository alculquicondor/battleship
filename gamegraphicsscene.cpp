#include "gamegraphicsscene.h"

GameGraphicsScene::GameGraphicsScene(QObject *parent, int rows, int columns) :
    QGraphicsScene(0, 0, (columns+1.5)*cellSize, (rows+1.5)*cellSize, parent),
    rows(rows), columns(columns), finished(false)
{
    QImage seaImage(":/resources/img/sea.png");
    QBrush bgBrush(seaImage);
    setBackgroundBrush(bgBrush);
    tablePen = new QPen(Qt::DashLine);
    tablePen->setWidth(2);
    tablePen->setColor(QColor(255, 255, 255, 80));
    addPath(TablePainterPath(rows, columns), *tablePen);
    missileIcon = new QPixmap(":/resources/img/missile.xpm");
    explosionIcon = new QPixmap(":/resources/img/explosion.xpm");
    splashIcon = new QPixmap(":/resources/img/splash.xpm");
    winIcon = new QPixmap(":/resources/img/victory.png");
    releaseSound = new Phonon::MediaSource(":/resources/sound/release.wav");
    explodeSound = new Phonon::MediaSource(":/resources/sound/explode.wav");
    splashSound = new Phonon::MediaSource(":/resources/sound/splash.wav");
    engine = new BattleShipEngine(rows, columns);
    engine->putShips(rows * columns / 25);
}

GameGraphicsScene::~GameGraphicsScene()
{
    delete engine;
    delete releaseSound;
    delete explodeSound;
    delete splashSound;
    delete tablePen;
    delete explosionIcon;
    delete missileIcon;
    delete winIcon;
    delete splashIcon;
}

void GameGraphicsScene::setRowsColumns(int rows, int columns)
{
    if (rows < 5 or columns < 5) {
        emit statusMessage("No es posible jugar con esas dimensiones");
        return;
    }
    clear();
    delete engine;
    this->rows = rows;
    this->columns = columns;
    finished = false;
    engine = new BattleShipEngine(rows, columns);
    engine->putShips(rows * columns / 25);
    setSceneRect(0, 0, (columns+1.5)*cellSize, (rows+1.5)*cellSize);
    addPath(TablePainterPath(rows, columns), *tablePen);
}

void GameGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    int tc = (int)(event->scenePos().x()/cellSize) - 1,
            tr = (int)(event->scenePos().y()/cellSize) - 1;
    if (not finished and tr >= 0 and tr < rows and tc >= 0 and tc < columns)
        play(tr, tc);
    QGraphicsScene::mousePressEvent(event);
}

void GameGraphicsScene::play(int row, int column)
{
    AttackResult result = engine->attack(row, column);
    if (result.state) {
        Phonon::MediaObject *player = Phonon::createPlayer(Phonon::MusicCategory, *releaseSound);
        connect(player, SIGNAL(aboutToFinish()), this, SLOT(soundAboutToFinish()));
        connect(player, SIGNAL(finished()), this, SLOT(soundFinishedPlaying()));
        AnimatedPictureItem *missile = new AnimatedPictureItem(missileIcon->size(), *missileIcon, this);
        addItem(missile);
        missile->setZValue(3);
        QPointF position(cellSize*(column+1), cellSize*(row+1));
        missile->animatePosition(position - QPointF(10*cellSize, 10*cellSize), position);
        if (result.state >= AttackResult::HIT) {
            connect(missile, SIGNAL(animationFinishedSignal()), this, SLOT(showExplosion()));
            player->enqueue(*explodeSound);
            if (result.destroyedShip) {
                emit statusMessage("Destruiste un " + result.destroyedShip->getName() +" en "+QString::number(engine->shot)+
                                   " disparos y te falta destruir ("+QString::number(engine->getShipsLeft()) +
                                   ") barcos.", 8000);
                Ship *ship = result.destroyedShip;
                AnimatedPictureItem *shipIcon = new AnimatedPictureItem(ship->getIcon()->size(),
                                                                        *ship->getIcon(), this);
                addItem(shipIcon);
                shipIcon->setZValue(1);
                if (ship->getDirection()) {
                    shipIcon->setRotation(90);
                    shipIcon->setPos(cellSize*(ship->getPosition().y()+2),
                                     cellSize*(ship->getPosition().x()+1));
                } else {
                    shipIcon->setPos(cellSize*(ship->getPosition().y()+1),
                                     cellSize*(ship->getPosition().x()+1));
                }
                shipIcon->animateOpacity(0.1, 1, 1000);
                if (result.state == AttackResult::WIN) {
                    AnimatedPictureItem *win = new AnimatedPictureItem(winIcon->size(), *winIcon, this);
                    addItem(win);
                    win->animateOpacity(0.2, 1, 3000);
                    win->setZValue(5);
                    win->setPos(cellSize*columns/2.-220, cellSize*rows/2.-50);
                    finished = true;
                }
            }
        } else {
            connect(missile, SIGNAL(animationFinishedSignal()), this, SLOT(showSplash()));
            player->enqueue(*splashSound);
        }
        player->play();
    }
}

void GameGraphicsScene::replaceNAnimate(AnimatedPictureItem *missile, QPixmap *icon)
{
    QPointF position = missile->pos();
    missile->animateOpacity(1, 0, 80);
    AnimatedPictureItem *item = new AnimatedPictureItem(icon->size(), *icon, this);
    addItem(item);
    item->setZValue(2);
    item->setPos(position);
    item->animateOpacity(0.1, 1, 450);
}

void GameGraphicsScene::soundFinishedPlaying()
{
    Phonon::MediaObject *mediaObject = qobject_cast<Phonon::MediaObject *>(sender());
    if (mediaObject) {
        mediaObject->stop();
        mediaObject->clearQueue();
        mediaObject->deleteLater();
    }
}

void GameGraphicsScene::soundAboutToFinish()
{
}

void GameGraphicsScene::showExplosion()
{
    AnimatedPictureItem *missile =  qobject_cast<AnimatedPictureItem *>(sender());
    disconnect(missile, SIGNAL(animationFinishedSignal()), this, SLOT(showExplosion()));
    replaceNAnimate(missile, explosionIcon);
}

void GameGraphicsScene::showSplash()
{
    AnimatedPictureItem *missile =  qobject_cast<AnimatedPictureItem *>(sender());
    disconnect(missile, SIGNAL(animationFinishedSignal()), this, SLOT(showSplash()));
    replaceNAnimate(missile, splashIcon);
}
