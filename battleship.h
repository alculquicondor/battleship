#ifndef BATTLESHIP_H
#define BATTLESHIP_H

#include "ship.h"

class BattleShip : public Ship
{
public:
    static const int LENGTH = 4;
    BattleShip(int row, int column, bool direction);
private:
    void f() {}
};

#endif // BATTLESHIP_H
