#ifndef NEWGAMEDIALOG_H
#define NEWGAMEDIALOG_H

#include <QDialog>
#include <QCloseEvent>

namespace Ui {
class NewGameDialog;
}

class NewGameDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit NewGameDialog(QWidget *parent = 0);
    ~NewGameDialog();
    int getRows();
    int getColumns();

private:
    Ui::NewGameDialog *ui;
};

#endif // NEWGAMEDIALOG_H
